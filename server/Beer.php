<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beer extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json; charset=utf-8');

    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    //curl -i -G -X POST --data "grant_type=client_credentials" -H "Authorization: Basic YWItMTAyOk15U2VjcmV0" https://api.foodily.com/v1/token
    public function auth()
    {
        $accessToken = "YWItMTk6b1BrRzJ5NmJMQVR5YlBCcw==";

        // Get cURL resource
        $curl = curl_init();
        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'https://api.foodily.com/v1/token',
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS =>
                    'grant_type=client_credentials'
            ,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Basic " . $accessToken)
            )
        );
        $resp = curl_exec($curl);
        curl_close($curl);


        $data["output"] = $resp;
        $this->load->view('output/txt', $data);
    }

    private function getOAuthToken()
    {
        $accessToken = "YWItMTk6b1BrRzJ5NmJMQVR5YlBCcw==";
        // Get cURL resource
        $curl = curl_init();
        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'https://api.foodily.com/v1/token',
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS =>
                    'grant_type=client_credentials'
            ,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Basic " . $accessToken)
            )
        );
        $resp = curl_exec($curl);
        curl_close($curl);

        $resp = json_decode($resp);
        return $resp->access_token;

    }

    public function getBeers(){
        $token = $this->getOAuthToken();

        $name = $this->input->get_post("name");
        $id = $this->input->get_post("id");
        $url="https://api.foodily.com/v1/beerLookup?zone=EUR&limit=50";

        if($name!=""){
           $url = $url."&name=$name";
        }

        if($id!=""){
            $url = $url."&id=$id";
        }
        // Get cURL resource
        $curl = curl_init();
        curl_setopt_array($curl, array(
                CURLOPT_HEADER => true,
                CURLOPT_URL =>$url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer " . $token),
                CURLOPT_HEADER => false
            )
        );
        $resp = curl_exec($curl);
        curl_close($curl);


    }

    //GET v1/beerPairings?q=chicken&offset=0&limit=1&fields=*(*),recipePairings(recipe(name,id,images(list(smallUrl)),sourceSite(name),sourceRecipe(url)),pairings(*))
    public function beerPairing()
    {

            $token = $this->getOAuthToken();
        $q = $this->input->get_post("food");
        if($q==""){
            $q = "popular";
        }
        $flavorProfile = $this->input->get_post("flavorProfile");
        $pairingType = $this->input->get_post("pairingType");

        if($pairingType==""){
            $pairingType ="all";
        }
        $url =  "https://api.foodily.com/v1/beerPairings?q=$q&pairingType=$pairingType&offset=0&limit=10&fields=*(*),recipePairings(recipe(name,id,images(list(smallUrl)),sourceSite(name),sourceRecipe(url)),pairings(*))";

        if($flavorProfile!=''){
            $url = $url ."&flavorProfile=$flavorProfile";
        }
        // Get cURL resource
        $curl = curl_init();
        curl_setopt_array($curl, array(
                CURLOPT_HEADER => true,
                CURLOPT_URL =>$url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer " . $token),
                CURLOPT_HEADER => false
            )
        );

        $resp = curl_exec($curl);
        curl_close($curl);

    }
}
