// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform,   $ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    var client;
    dataArray = [];

    function doConnect() {
      var clientId = "test_" + new Date().getTime()
      client = new Messaging.Client("pubsub.evrythng.com", 80, clientId);
      console.log("Client instantiated.");
      client.startTrace();
      console.log("Now trying to connect...");
      client.onMessageArrived = onMessageArrived;
      client.connect({
        onSuccess: onConnect
      });
    }

    function onConnect() {
      console.log("connection established");
      doSubscribe();
    }

    function doSubscribe() {
      var apiKey = "MYtbOR7JM2MKoh83zpae93FM2ZGEtY0YpxM1tu0TydFiGxqovx4fuWiMRu9qY2GD6iOZSakHwTepEgmb";
      var itemToSubscribe = "products/UB7gNkXd8epapGM8aYgdna7c/properties/msg"
      client.subscribe(itemToSubscribe + "?access_token=" + apiKey);
    }

    function onMessageArrived(message) {
      //console.log("onMessageArrived:"+ message.payloadString);
      console.log(message.payloadString[0]);
      v = JSON.parse(message.payloadString)


      var alertPopup = $ionicPopup.alert({
        title: 'Beerweiser',
        template:  v[0].value
      });
      alertPopup.then(function(res) {
        console.log('Thank you for not eating my delicious ice cream cone');
      });
      dataArray.push([0, 1]);
    };


    doConnect();
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.scan', {
    url: '/scan',
    views: {
      'tab-scan': {
        templateUrl: 'templates/tab-scan.html',
        controller: 'ScanCtrl'
      }
    }
  })

  .state('tab.search', {
    url: '/search',
    views: {
      'tab-search': {
        templateUrl: 'templates/tab-search.html',
        controller: 'SearchCtrl'
      }
    }
  })

  .state('tab.map', {
      url: '/map',
      views: {
        'tab-map': {
          templateUrl: 'templates/tab-map.html',
          controller: 'MapCtrl'
        }
      }
    })

  .state('tab.badges', {
      url: '/badges',
      views: {
        'tab-badges': {
          templateUrl: 'templates/tab-badges.html',
          controller: 'BadgesCtrl'
        }
      }
    })

  .state('tab.handbook', {
    url: '/handbook',
    views: {
      'tab-handbook': {
        templateUrl: 'templates/tab-handbook.html',
        controller: 'HandbookCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/scan');

});
