angular.module('starter.services', [])

.factory('ApiModule', function() {

  function scan() {
    var API_KEY = "MYtbOR7JM2MKoh83zpae93FM2ZGEtY0YpxM1tu0TydFiGxqovx4fuWiMRu9qY2GD6iOZSakHwTepEgmb";

    // Initialise Evrythng.js App
    var app = new EVT.App(API_KEY);
    // Initialise Scanthng
    var st = new EVT.ScanThng(app);

    var beerId;
    /*
     * We use a promise but callbacks are also supported:
     * s.identify(options, successCb, errorCb);
     */
     return st.identify({
        type: 'objpic', // options are: objpic, 1dbarcode, qrcode
        redirect: false // 'true' means we get redirected to the App
          // corresponding to this Product
        })
     .then(function(photoData) {
        // Do something with the results, like loading the Product/Thng
        // information from the EVRYTHNG API
        console.info('SUCCESS', photoData);

        beerId = getBeerDataId(photoData.redirectUrl);

        return Q(jQuery.ajax({
          url: photoData.evrythngUrl + "?access_token=" + API_KEY,
          type: "GET"
        })).then(function(data) {
          // on success
          return data;
        }, function(xhr) {
          // on failure
          throw xhr;
        });
      })
     .then(function(beerData) {
      console.info("Success 2", beerData);

      if (!beerId) {
        throw "beerId is not created";
      }

      return Q(jQuery.ajax({
        url: "http://sharechiwai.ngrok.com/hack/beer/getBeers?id=" + beerId,
        type: "GET"
      })).then(function(matchData) {
        console.info("matchData: ", matchData);
          // on success
          return parseData(beerData, matchData.beers[0]);
        });
    }, function(error) {
      console.error(error);
      var errorMsg = error.message ? error.message : "Unknown message";

      return {
        error: errorMsg
      };
    });
   }

   function getBeerDataId(url) {
    return url.match(/\/info\/(.*)/)[1];
  }

  function parseData(beerData, matchData) {
    return {
      brand: beerData.brand,
      photos: beerData.photos,
      description: matchData.description,
      flavorProfile: matchData.flavorProfile,
      abv: matchData.abv.toFixed(2)
    };
  }

  function search(keyword) {
    return Q(jQuery.ajax({
      url: "http://sharechiwai.ngrok.com/hack/beer/getBeers?food=" + keyword,
      type: "GET"
    })).then(function(matchData) {
      console.info("Search matchData: ", matchData);
      // on success
      return matchData.beers.slice(0, 10);
    });
  }


  function pushMessage() {
    var client;
    dataArray = [];

    function doConnect() {
      var clientId = "test_" + new Date().getTime()
      client = new Messaging.Client("pubsub.evrythng.com", 80, clientId);
      console.log("Client instantiated.");
      client.startTrace();
      console.log("Now trying to connect...");
      client.onMessageArrived = onMessageArrived;
      client.connect({
        onSuccess: onConnect
      });
    }

    function onConnect() {
      console.log("connection established");
      doSubscribe();
    }

    function doSubscribe() {
      var apiKey = "MYtbOR7JM2MKoh83zpae93FM2ZGEtY0YpxM1tu0TydFiGxqovx4fuWiMRu9qY2GD6iOZSakHwTepEgmb";
      var itemToSubscribe = "products/UB7gNkXd8epapGM8aYgdna7c/properties/msg"
      client.subscribe(itemToSubscribe + "?access_token=" + apiKey);
    }

    function onMessageArrived(message) {
      //console.log("onMessageArrived:"+ message.payloadString);
      console.log(message.payloadString[0]);
      v = JSON.parse(message.payloadString)
      alert(v[0].value);
      showMessage(message.payloadString);
      dataArray.push([0, 1]);
    };

    function showMessage(message) {
      $('#messages').append("<li class='list-group-item'>" + message + "</li>");
    }

    doConnect();
  }


  return {
    scan: scan,
    search: search,
  }
});