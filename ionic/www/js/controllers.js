angular.module('starter.controllers', ['ionic'])

.controller('ScanCtrl', function($scope, ApiModule) {
  $scope.startScan = function() {
    $scope.errorMsg = "";
    ApiModule.scan().then(function(data) {
      console.info(data);

      if (data.error) {
        $scope.errorMsg = "Server error. Try again later";
      } else {
        $scope.beerName = data.brand;
        $scope.beerImg = data.photos && data.photos[0];
        $scope.beerDescription = data.description;
        $scope.beerFavour = data.flavorProfile;
        $scope.beerAbv = data.abv;
      }
        $scope.$digest();
    });
  };
})

.controller('SearchCtrl', function($scope, ApiModule) {

  function searchViewModel(){
    var self = this;
    self.searchVal = "";

    self.search = function() {
      ApiModule.search(self.searchVal).then(function(beerArr) {
        console.log(beerArr);
        self.beerArr = beerArr;

        $scope.$digest();
      });
    };
  }

  $scope.model = new searchViewModel();

})

.controller('MapCtrl', function($scope) {})

.controller('BadgesCtrl', function($scope) {})

.controller('HandbookCtrl', function($scope) {
  $scope.handbookDetail = function() {
    //$('ion-nav-view[name="tab-handbook"] ion-content').toggleClass("clicked");
    var fromImgSrc = $('ion-nav-view[name="tab-handbook"] img').attr("src");
        toImgSrc = (fromImgSrc.indexOf("handbookdetail.jpg") > -1) ? "img/handbook.jpg" : "img/handbookdetail.jpg";

      $('ion-nav-view[name="tab-handbook"] img').attr("src", toImgSrc);
  }
});
